# coding: utf8

""""""

import platform
from importlib import import_module

try:
    systems = {"Darwin": "osx"}
    system = platform.system()
    snowboydecoder = import_module(f"snowboy.{systems.get(system)}.snowboydecoder")
except Exception as e:
    print(e)
